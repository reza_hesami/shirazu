# Shirazu


## Usage

```c++
#include <Arduino.h>
#include <Shirazu.h>

Shirazu shirazu;
void setup(){
    // connect to WiFi
    // ...
    shirazu.login("username", "password");
    // ...
    // ...
    shirazu.logout();
}
void loop(){
    
}
```