#include "Shirazu.h"

const char *server = "login.shirazu.ac.ir";

const char *rootCA =
    "-----BEGIN CERTIFICATE-----\n"
    "MIIDuzCCAqOgAwIBAgIDBETAMA0GCSqGSIb3DQEBBQUAMH4xCzAJBgNVBAYTAlBM\n"
    "MSIwIAYDVQQKExlVbml6ZXRvIFRlY2hub2xvZ2llcyBTLkEuMScwJQYDVQQLEx5D\n"
    "ZXJ0dW0gQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxIjAgBgNVBAMTGUNlcnR1bSBU\n"
    "cnVzdGVkIE5ldHdvcmsgQ0EwHhcNMDgxMDIyMTIwNzM3WhcNMjkxMjMxMTIwNzM3\n"
    "WjB+MQswCQYDVQQGEwJQTDEiMCAGA1UEChMZVW5pemV0byBUZWNobm9sb2dpZXMg\n"
    "Uy5BLjEnMCUGA1UECxMeQ2VydHVtIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MSIw\n"
    "IAYDVQQDExlDZXJ0dW0gVHJ1c3RlZCBOZXR3b3JrIENBMIIBIjANBgkqhkiG9w0B\n"
    "AQEFAAOCAQ8AMIIBCgKCAQEA4/t9o3K6wvDJFIf1awFO4W5AB7ptJ11/91sts1rH\n"
    "UV+rpDKmYYe2bg+G0jACl/jXaVehGDldamR5xgFZrDwxSjh80gTSSyjoIF87B6LM\n"
    "TXPb865Px1bVWqeWifrzq2jUI4ZZJ88JJ7ysbnKDHDBy3+Ci6dLhdHUZvSqeexVU\n"
    "BBvXQzmtVSjF4hq79MDkrjhJM8x2hZ85RdKknvISjFH4fOQtf/WsX+sWn7Et0brM\n"
    "kUJ3TCXJkDhv2/DM+44el1k+1WBO5gUo7Ul5E0u6SNsv+XLTOcr+H9g0cvW0QM8x\n"
    "AcPs3hEtF10fuFDRXhmnad4HMyjKUJX5p1TLVIZQRan5SQIDAQABo0IwQDAPBgNV\n"
    "HRMBAf8EBTADAQH/MB0GA1UdDgQWBBQIds3LB/8k9sXN7buQvOKEN0Z19zAOBgNV\n"
    "HQ8BAf8EBAMCAQYwDQYJKoZIhvcNAQEFBQADggEBAKaorSLOAT2mo/9i0Eidi15y\n"
    "sHhE49wcrwn9I0j6vSrEuVUEtRCjjSfeC4Jj0O7eDDd5QVsisrCaQVymcODU0HfL\n"
    "I9MA4GxWL+FpDQ3Zqr8hgVDZBqWo/5U30Kr+4rP1mS1FhIrlQgnXdAIv94nYmem8\n"
    "J9RHjboNRhx3zxSkHLmkMcScKHQDNP8zGSal6Q10tz6XxnboJ5ajZt3hrvJBW8qY\n"
    "VoNzcOSGGtIxQbovvi0TWnZvTuhOgQ4/WwMioBK+ZlgRSssDxLQqKi2WF+A5VLxI\n"
    "03YnnZotBqbJ7DnSq9ufmgsnAjUpsUCV5/nonFWIGUbWtzT1fs45mtk48VH3Tyw=\n"
    "-----END CERTIFICATE-----\n";

Shirazu::Shirazu(String username, String password)
{
    this->username = username;
    this->password = password;
}
Shirazu::Shirazu() {}

int8_t Shirazu::login(String username, String password)
{
    client.setCACert(rootCA);
    if (!client.connect(server, 443))
        return -1;
    
    String payload = "dst=&popup=true&error_code=&username=" + urlEncode(username) + "&password=" + urlEncode(password);
    int payloadSize = payload.length();
    client.println("POST /login HTTP/1.1");
    client.println("Host: login.shirazu.ac.ir");
    client.println("Connection: close");
    client.println("Content-Length: " + String(payloadSize));
    client.println();
    client.println(payload);
    client.println();
    String response;
    
    unsigned long startTime = millis();
    while (client.connected())
    {
        if (millis() - startTime > 5000)
            return -1;
        response = client.readString();
    }
    client.stop();

    if(response.indexOf("Simulation exceed") > 0)
        return -2;
    if(response.indexOf("SSHA password check failed") > 0)
        return -3;
    if(response.indexOf("http://login.shirazu.ac.ir/status") > 0) // fuck backend of sess
        return 1;
    return -4;
}
int8_t Shirazu::login()
{
    return this->login(this->username, this->password);
}

int8_t Shirazu::logout()
{
    client.setCACert(rootCA);
    if (!client.connect(server, 443))
        return -1;
    
    client.println("GET /logout HTTP/1.1");
    client.println("Host: login.shirazu.ac.ir");
    client.println("Connection: keep-alive");
    client.println();
    unsigned long startTime = millis();
    String response;
    while (client.connected())
    {
         if (millis() - startTime > 5000)
             return -2;
        response = client.readString();

    }
    client.stop();
    return 1;
}

Shirazu shirazu;